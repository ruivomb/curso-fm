package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

import org.br.curso.pages.CmsPage;
import org.br.curso.pages.HomePage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;

@DesignSteps
public class CmsSteps {

	HomePage homePage = getPage_(HomePage.class);
	CmsPage cmsPage = getPage_(CmsPage.class);
	
	@Step("E deletar um cms")
	public void deletar_cms() throws ElementFindException, GenericException {
		homePage.validar_pagina();
		homePage.cms();
		homePage.page();
		cmsPage.deletando();
		cmsPage.clicar_ok();
	}
	
	@Step("Ent�o o CMS ser� apagado")
	public void validar_exclusao() throws ElementFindException {
		cmsPage.validar_deletar();
	}
}
