package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

import org.br.curso.pages.HomePage;
import org.br.curso.pages.LoginPage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

@DesignSteps
public class LogoutSteps {
	
	HomePage homePage = getPage_(HomePage.class);
	LoginPage loginPage = getPage_(LoginPage.class);
	
	@Step("Dado que eu esteja na home do phpravels")
	public void estar_na_home() {
		homePage.validar_pagina();
	}
	
	@Step("Quando eu clicar em logout")
	public void clicar_em_logout() throws ElementFindException {
		homePage.clicar_em_logout();
	}
	
	@Step("Ent�o sairei da minha conta")
	public void sair_da_conta() {
		loginPage.validar_logout();
	}

}
