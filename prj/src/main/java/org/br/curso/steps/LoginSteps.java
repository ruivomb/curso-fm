package org.br.curso.steps;

import org.br.curso.pages.LoginPage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

@DesignSteps
public class LoginSteps {
	
	LoginPage loginPage = getPage_(LoginPage.class);
	
	
	@Step("Dado que eu estou na p�gina do phptravels")
	public void dado_que_estou_na_pagina_do_phpwebtravels() throws ElementFindException {
		loginPage.validar_pagina();
		
	}
	
	@Step("Quando acessar o login com usu�rio v�lido")
	public void quando_efetuar_o_login_com_usuario_valido() throws ElementFindException {
		loginPage.preencher_login("admin@phptravels.com", "demoadmin");
		
	}
	
}
