package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

import org.br.curso.pages.AccountPage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;

@DesignSteps
public class DelAccountSteps {

	AccountPage accountPage = getPage_(AccountPage.class);
	
	@Step("clicar em deletar")
	public void clicar_em_del() throws ElementFindException {
		accountPage.deletando_costumer();
	}
	
	@Step("Confirmar a exclus�o")
	public void clicar_em_ok() throws ElementFindException, GenericException {
		accountPage.confirmar();
	}
	
	@Step("Ent�o o costumer sera deletado")
	public void validar_que_foi_deletado() throws ElementFindException {
		accountPage.validar_deletar();
	}
}
