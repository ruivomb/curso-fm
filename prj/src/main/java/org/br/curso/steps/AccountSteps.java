package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import static org.testng.Assert.assertTrue;

import org.br.curso.pages.AccountPage;
import org.br.curso.pages.HomePage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;


@DesignSteps
public class AccountSteps {
	
	HomePage homePage = getPage_(HomePage.class);
	AccountPage accountPage = getPage_(AccountPage.class);
	
	@Step("E cadastrar costumer")
	public void e_cadastrar_costumer() throws ElementFindException {
		homePage.validar_pagina();
		homePage.acessar_costumer();
		accountPage.clicar_em_add();
		accountPage.validar_pagina_Add_Customers();
		accountPage.preencher_formulario("Mateus", "balduino", "mateus@gmail.com", "123456", "987654321", "", "rua das adalias", "rua do cravo");
		accountPage.enviar_cadastro();
	}
	
	@Step("Ent�o o costumer sera cadastrado")
	public void entao_customer_sera_cadastrado() throws ElementFindException {
		assertTrue(accountPage.valid_customerAdd("Mateus"));
	}	
}