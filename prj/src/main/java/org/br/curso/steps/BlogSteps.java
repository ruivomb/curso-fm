package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import static org.testng.Assert.assertTrue;

import org.br.curso.pages.BlogPage;
import org.br.curso.pages.HomePage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

@DesignSteps
public class BlogSteps {

	HomePage homePage = getPage_(HomePage.class);
	BlogPage blogPage = getPage_(BlogPage.class);

	@Step("clicar no bot�o blog")
	public void clicar_em_blog() throws ElementFindException {
		homePage.validar_pagina();
		blogPage.clicar_blog();
	}
	
	@Step("clicar em add")
	public void clicar_add() throws ElementFindException {
		blogPage.clicar_add();
	}
	
	@Step("preencher os dados")
	public void preencher() throws ElementFindException {
		blogPage.preencher_campos("Titulo", "Link qualquer", "Palavra-chave", "Descri��o");
		blogPage.clicar_submit();
	}
	
	@Step("Ent�o o post ser� adicionado")
	public void entao_customer_sera_cadastrado() throws ElementFindException {
		assertTrue(blogPage.validar_post("Titulo"));
	}
	
}
