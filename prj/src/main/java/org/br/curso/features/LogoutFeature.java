package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;
import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("Logout")
public class LogoutFeature {

	@SuppressWarnings("static-access")
	@Scenario("Realizar logout")
	public void logout() throws ExecutionException {
		given_("Dado que eu estou na p�gina do phptravels").
		when_("Quando acessar o login com usu�rio v�lido").
		and_("Quando eu clicar em logout").
		then_("Ent�o sairei da minha conta").
		execute_();
		}	
}
