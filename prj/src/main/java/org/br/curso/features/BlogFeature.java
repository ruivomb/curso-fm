package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("Blog")
public class BlogFeature {

	@SuppressWarnings("static-access")
	@Scenario("Adicionar post")
	public void executar_login_valido() throws ExecutionException {
		
		given_("Dado que eu estou na p�gina do phptravels").
		when_("Quando acessar o login com usu�rio v�lido").
		and_("clicar no bot�o blog").
		and_("clicar em add").
		and_("preencher os dados").
		then_("Ent�o o post ser� adicionado")
		.execute_();
		
	}
	
}
