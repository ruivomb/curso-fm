package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("Login")
public class LoginfFeature {

	@SuppressWarnings("static-access")
	@Scenario("Executar login com usu�rio v�lido")
	public void executar_login_valido() throws ExecutionException {
		
		given_("Dado que eu estou na p�gina do phptravels").
		when_("Quando acessar o login com usu�rio v�lido").
		then_("Ent�o a home ser� exibida")
		.execute_();
		
	}
	
}
