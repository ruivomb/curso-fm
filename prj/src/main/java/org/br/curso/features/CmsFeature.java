package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("Deletar")
public class CmsFeature {

	@SuppressWarnings("static-access")
	@Scenario("Deletar CMS")
	public void deletar_cms() throws ExecutionException {
		
		given_("Dado que eu estou na p�gina do phptravels").
		when_("Quando acessar o login com usu�rio v�lido").
		and_("E deletar um cms").
		then_("Ent�o o CMS ser� apagado")
		.execute_();
		
	}
	
}