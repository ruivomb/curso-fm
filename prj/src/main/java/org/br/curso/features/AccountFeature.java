package org.br.curso.features;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

@Feature("Cadastrar")
public class AccountFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Cadastrar costumer")
	public void executar_cadastro() throws ExecutionException {
		
		given_("Dado que eu estou na p�gina do phptravels").
		when_("Quando acessar o login com usu�rio v�lido").
		and_("E cadastrar costumer").
		then_("Ent�o o costumer sera cadastrado")
		.execute_();
		
	}

}
