package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class CmsPage extends PageBase{
	VirtualElement 
				   btnDel = getElementByXPath("//a[@href=\"javascript: delfunc('68','https://www.phptravels.net/admin/ajaxcalls/delPage')\"]"),
				   lblCms = getElementByXPath("//div[text()=\"CMS Management\"]");
	
	LoggerHelper logger = new LoggerHelper(CmsPage.class);
	
	
	
	public void deletando() throws ElementFindException {
		if(elementExists(btnDel)) {
			logger.info("deletando.", true);
			btnDel.click();
		}else {
			logger.info("Bot�o n�o encontrado.");
		}
	}
	
	public void clicar_ok() throws ElementFindException, GenericException {
		getController().getDriver_().switchTo().alert().accept();
	}
	
	public void validar_deletar() {
		Assert.assertTrue(elementExists(lblCms));		
		logger.info("Deu tudo certo.", true);
	}
}
