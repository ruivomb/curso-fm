package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;
import org.testng.Assert;

public class HomePage extends PageBase{
	VirtualElement lblDashboard = getElementByXPath("//p[text() = 'DASHBOARD']"),
			       btnLogout = getElementByXPath("//a[@href=\"https://www.phptravels.net/admin/logout\"]"),
			       lnkAccount = getElementByXPath("//ul[@id=\"ACCOUNTS\"]/.."),
			       lnkCostumer = getElementByXPath("//a[@href=\"https://www.phptravels.net/admin/accounts/customers/\"]"),
	               btnCms = getElementByXPath("//a[@href=\"#CMS\"]"),
			       btnPage = getElementByXPath("//a[text()=\"Pages\"]");
	
	LoggerHelper logger = new LoggerHelper(HomePage.class);
	
	public void validar_pagina () {
		
		Assert.assertTrue(elementExists(lblDashboard));		
		logger.info("Pagina inicial foi encontrada.", true);
		
	}
	
	public void acessar_costumer() throws ElementFindException {
		lnkAccount.click();
		lnkCostumer.click();
				
	}
	
	public void cms() throws ElementFindException {
		if(elementExists(btnCms)) {
			logger.info("Clicando em cms.", true);
			btnCms.click();
		}else {
			logger.info("Bot�o n�o encontrado.");
		}
	}
	
	public void page() throws ElementFindException {
		if(elementExists(btnPage)) {
			logger.info("Clicando em page.", true);
			btnPage.click();
		}else {
			logger.info("Bot�o n�o encontrado.");
		}
	}
	
	public void clicar_em_logout() throws ElementFindException {
		if(elementExists(btnLogout)) {
			logger.info("Clicando em logout.", true);
			btnLogout.click();
		}else {
			logger.info("Bot�o n�o foi encontrado.");
		}
		
	}
}
