package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

public class BlogPage extends PageBase {
	VirtualElement btnBlog = getElementByXPath("//div[text()=\" Blog\"]"),
				   btnAdd = getElementByXPath("//button[text()=\" Add\"]"),
				   txtTitle = getElementByXPath("//input[@name=\"title\"]"),
				   txtLink = getElementByXPath("//iframe[@title='Rich Text Editor, desc']"),
				   txtKey = getElementByXPath("//input[@name=\"keywords\"]"),
				   txtDescription = getElementByXPath("//input[@name=\"metadesc\"]"),
				   slctCategory = getElementByXPath("//option[text()=\"Select\"]/.."),
				   slctTravel = getElementByXPath("//option[@value=\"15\"]"),
				   btnSubmit = getElementByXPath("//input[@name=\"metadesc\"]");
	
	LoggerHelper logger = new LoggerHelper(BlogPage.class);
	
	public void clicar_blog() throws ElementFindException {
		if(elementExists(btnBlog)) {
			logger.info("Clicando no blog", true);
			btnBlog.click();
		}else {
			logger.info("Bot�o n�o encontrado");
		}
	}
	
	public void clicar_add() throws ElementFindException {
		if(elementExists(btnAdd)) {
			logger.info("Adicionando", true);
			btnAdd.click();
		}else {
			logger.info("Bot�o n�o encontrado");
		}
	}
	
	public void preencher_campos(String title, String link, String keywords, String description) throws ElementFindException {
		if(elementExists(txtTitle)&& elementExists(txtLink)&& elementExists(txtKey)&& elementExists(txtDescription)) {
			
			txtTitle.sendKeys(title);
			txtLink.sendKeys(link);
			txtKey.sendKeys(keywords);
			txtDescription.sendKeys(description);
			slctCategory.click();
			slctTravel.click();
			
			logger.info("campos preenchidos", true);
		}else {
			logger.info("n�o foi preenchido");
		}		
	}
	
	public void clicar_submit() throws ElementFindException {
		btnSubmit.click();
	}

	public boolean validar_post(String title) {
		VirtualElement  valTitle = getElementByXPath("//td[text()='"+title+"']");
		return elementExists(valTitle);
	}
	
}








