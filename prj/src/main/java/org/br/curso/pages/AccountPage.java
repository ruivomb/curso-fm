package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class AccountPage extends PageBase{
	VirtualElement lblCustomersManagement = getElementByXPath("//div[@class = 'panel-heading']"),
				   btnAdd = getElementByXPath("//i[@class = 'glyphicon glyphicon-plus-sign']/.."),
				   lblAddCustomers = getElementByXPath("//div[@class = 'panel-heading']"),
				   txtFirstName = getElementByXPath("//input[@name = 'fname']"),
				   txtLastName = getElementByXPath("//input[@name = 'lname']"),
				   txtEmail = getElementByXPath("//input[@name = 'email']"),
				   txtPassword = getElementByXPath("//input[@name = 'password']"),
				   txtMobile = getElementByXPath("//input[@name = 'mobile']"),
				   cbxCountry = getElementByXPath("//span[@class = 'select2-chosen']"),
				   txtCountry = getElementByXPath("//li[@class = 'select2-results-dept-0 select2-result select2-result-selectable'][1]"),
				   txtAddress1 = getElementByXPath("//input[@name = 'address1']"),
				   txtAddress2 = getElementByXPath("//input[@name = 'address2']"),
				   ckbEmail = getElementByXPath("//input[@name = 'newssub']"),
				   btnSubmit = getElementByXPath("//button[text() = 'Submit']"),
				   btnDeletar = getElementByXPath("//a[@id=\"41\"]"),
				   lblCostumer = getElementByXPath("//div[text()=\"Customers Management\"]");
			   

	LoggerHelper logger = new LoggerHelper(AccountPage.class);
	
	public void clicar_em_add() throws ElementFindException {
		if(elementExists(btnAdd)) {
			logger.info("Clicando no boto Add.", true);
			btnAdd.click();
		}else {
			logger.info("Boto Add no foi encontrado");
		}
	}	
	
	 public void validar_pagina_Add_Customers() {
		
		if(elementExists(lblAddCustomers)) {
			Assert.assertTrue(elementExists(lblAddCustomers));
			logger.info("Pagina de Customers Management foi encontrada.", true);
		}else {
			logger.info("Add Customers no encontrada");
		}
	}

	 public void preencher_formulario(String firstName, String lastName, String email, 
		String password, String mobile, String country, String address1, String address2) throws ElementFindException {
	if(elementExists(txtFirstName)&& elementExists(txtLastName) && elementExists(txtEmail) 
			&& elementExists(txtPassword) && elementExists(txtMobile)
			&& elementExists(txtAddress1) && elementExists(txtAddress2)) {
		
		
		txtFirstName.sendKeys(firstName);
		txtLastName.sendKeys(lastName);
		txtEmail.sendKeys(email);
		txtPassword.sendKeys(password);
		txtMobile.sendKeys(mobile);
		cbxCountry.click();
		txtCountry.click();
		txtAddress1.sendKeys(address1);
		txtAddress2.sendKeys(address2);
		ckbEmail.click();
		
		logger.info("Formul�rio preenchido", true);
	
		
		
	}else {
		logger.info("Os campos no foram preenchidos");
	}
}

	 public void enviar_cadastro() throws ElementFindException {	
	btnSubmit.click();
}
	 
	 public boolean valid_customerAdd(String firstName) {
			VirtualElement  tdFirstName = getElementByXPath("//td[text()='"+firstName+"']");
			return elementExists(tdFirstName);
			
		}
	 
	 public void deletando_costumer() throws ElementFindException {
			if(elementExists(btnDeletar)) {
				logger.info("deletando.", true);
				btnDeletar.click();
			}else {
				logger.info("Bot�o n�o encontrado.");
			}
		}
		
		public void confirmar() throws ElementFindException, GenericException {
			getController().getDriver_().switchTo().alert().accept();
		}
		
		public void validar_deletar() {
			Assert.assertTrue(elementExists(lblCostumer));		
			logger.info("Deu tudo certo.", true);
		}
	}